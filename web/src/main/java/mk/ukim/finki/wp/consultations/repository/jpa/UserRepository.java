package mk.ukim.finki.wp.consultations.repository.jpa;

import mk.ukim.finki.wp.consultations.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {

    User findByUsername(String username);

}
