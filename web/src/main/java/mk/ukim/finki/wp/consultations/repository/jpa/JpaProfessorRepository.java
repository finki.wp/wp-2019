package mk.ukim.finki.wp.consultations.repository.jpa;

import mk.ukim.finki.wp.consultations.model.Professor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaProfessorRepository  extends JpaRepository<Professor, String> {
}
