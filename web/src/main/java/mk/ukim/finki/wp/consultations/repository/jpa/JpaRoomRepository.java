package mk.ukim.finki.wp.consultations.repository.jpa;

import mk.ukim.finki.wp.consultations.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JpaRoomRepository extends JpaRepository<Room, String> {
}
