More detailed explanation about the concepts in this lecture can be found in **Spring in Action (5th edition)**
 - **Chapter2**: Developing web applications

# Create new room 
1. Create `RoomsController` and annotate it with `@Controller`
2. In order to eliminate copy-paste of the whole html in every page, extract the repeating code in `master-template.html`
    - use `<div th:include="${bodyContent}"></div>` as a placeholder for the content body
3. Create a page for entering information about new room: 
    - Create a handler method in the `RoomsController`
    ```java
    @RequestMapping(value = "/rooms/new", method = RequestMethod.GET)
    public ModelAndView showCreateRoom() {
        ModelAndView modelAndView = new ModelAndView("master-template");
        modelAndView.addObject("bodyContent", "create-room");
        return modelAndView;
    }
    ```
    - create the `create-room.html` template, that will be displayed inside the `master-template.html`: 
    ```xhtml
    <form action="/rooms/create" 
          method="POST" xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
    
        <div class="form-group">
            <label for="roomBuilding">Room Name</label>
            <select name="building"
                    id="roomBuilding"
                    required="true"
                    class="form-control">
                <option th:each="building: ${T(mk.ukim.finki.wp.consultations.model.Building).values()}"
                        th:value="${building}"
                        th:text="${building.getDescription()}"></option>
    
            </select>
        </div>
    
        <div class="form-group">
            <label for="roomName">Room Name</label>
            <input type="text"
                   class="form-control"
                   required="true"
                   id="roomName" placeholder="Room Name">
        </div>
        <div class="form-group">
            <label for="roomDescription">Room Name</label>
            <textarea name="description"
                      id="roomDescription"
                      class="form-control"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    ```
 4. Implement the creation of the new room: 
    ```java
    
    @PostMapping("/rooms/create")
    public String createRoom(@RequestParam String name,
                             @RequestParam Building building,
                             @RequestParam String description) {
        roomService.createRoom(name, building, description);
        // other ways for redirection: https://www.baeldung.com/spring-redirect-and-forward
        return "redirect:/rooms";
    }
    ```

# Edit existing room
1. Create handler method that will show a form for room editing
    ```java
    @GetMapping("/rooms/{name}")
    public ModelAndView showEditRoom(@PathVariable String name) {
        Optional<Room> room = roomService.findByName(name);

        ModelAndView modelAndView = new ModelAndView("master-template");
        modelAndView.addObject("bodyContent", "edit-room");
        modelAndView.addObject("room", room.orElseThrow(InvalidRoomName::new));
        return modelAndView;
    }
    ```
   - create the service method `RoomService.findByName` and implement it by delegating to `RoomRepository.findById`
   - annotate the `InvalidRoomException` with `@ResponseStatus(HttpStatus.NOT_FOUND)` to return the corresponding status code
   - create a default constructor in `InvalidRoomException` that will set the message to the class name using: `super(InvalidRoomName.class.getSimpleName());` 
2. Create the `edit-room.html` template (reuse and adapt the code from `create-room.html`)
3. Update the `rooms.html` template such that the edit button will display the room editing page. Use the following
attribute for dynamic urls: `th:href="@{/rooms/{name}(name=${room.name})}"` 
4. Implement the room editing action: 
    ```java
       @PostMapping("/rooms/{oldName}")
       public String updateRoom(@PathVariable String oldName,
                                 @RequestParam String newName,
                                 @RequestParam Building building,
                                 @RequestParam String description) {
            roomService.updateRoom(oldName, newName, building, description);
            return "redirect:/rooms";   
       }
    ```

# Create a custom error page  
1. Create a template `error.html` that will display the errors. Copy the master template and change the dynamic placeholder
with the following content:
    ```xhtml
           <div id="block_error">
                <div>
                    <h2>Error <span th:text="${status}"></span>. &nbspOops you've have encountered the error:</h2>
                    <h4 th:text="${message}"></h4>
                </div>
           </div>
    ``` 
   
# Delete existing room 

1. Change the link for deleting room in `rooms.html` with the following form: 
    ```xhtml
    <form method="POST"
          th:action="@{/rooms/{name}/delete(name=${room.name})}">
        <button class="btn btn-danger" title="Избриши">
            <i class="fa fa-fw fa-trash"></i>
        </button>
    </form>
    ```
2. Implement the handler for deleting rooms: 
    ```java
       @PostMapping("/rooms/{name}/delete")
       public String deleteRoom(@PathVariable String name) {
            roomService.deleteRoom(name);
            return "redirect:/rooms";
       }
    ```
3. Add confirmation dialog before deleting the rooms. Add the following attribute in the form that invokes the deleting
`onsubmit="return confirm('Are you sure that you whant to delete this room?')"`

# Display rooms with mvc controller

1. Annotate `RoomThymeleafServlet` with `@Profile("servlet")`
    - After this action, the access to `/rooms` will dislay an `404 error`, because Spring is using the `default` profile 
    if not configured differently, and this profile will not inject the *Beans* annotated with other profile. 
    - The beans without `@Profile` annotation will be always injected into the context
    - The beans with `@Profile` annotation will be injected only when the configured profile is active
2. Create handler that will return the rooms
    ```java    
    @GetMapping("/rooms")
    public String showRooms(HttpServletRequest request,
                            @RequestParam(required = false) String query) {
        List<Room> rooms = query == null || query.isEmpty() ?
                this.roomService.getAllRooms() : this.roomService.searchRooms(query);

        request.setAttribute("rooms", rooms);
        request.setAttribute("bodyContent", "rooms");
        request.setAttribute("query", query);
        return "master-template";
    }
    ```
   - Refactor the template `rooms.html` such that only the `<div class="room">...</div>` will remain in the file
   - The previous method has an optional query parameter used to select the behaviour from the service layer
3. In `master-template.html` add `name="query"` and `th:value="${query}"` attributes to the search input element 

# Display rooms grouped by building

1. Group rooms by building in the `showRooms` handler: 
    ```java
    Map<Building, List<Room>> buildingRooms = rooms.stream().collect(Collectors.groupingBy(Room::getBuilding));
    ```
2. Update the `rooms.html` template to show the rooms grouped by the building: 
    ```xhtml
    <div class="room mt-3"
         method="POST" xmlns="http://www.w3.org/1999/xhtml"
         xmlns:th="http://www.thymeleaf.org"
        th:each="entry: ${buildingRooms}">
    
        <h3 th:text="${entry.key.getDescription()}"></h3>
    
        <div class="row">
            <th:block th:each="room : ${entry.value}">
               ...
           </th:block>
        </div>
    </div>
    ```