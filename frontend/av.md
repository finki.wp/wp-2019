# Create react app

`npx create-react-app consultations-react`

# Open an existing app

1. Download the application 
2. Synchronize the dependencies using `npm install`

# 




## Additional Dependencies

    
    "moment": "^2.24.0",
    "react-moment": "^0.9.6",
    "font-awesome": "^4.7.0"
    
addf
       
    import Header from '../Header/header';
    import ConsultationTerm from '../ConsultationTerm/consultationTerm';
    
    import * as consultationsRepository from "../../repository/consultationsRepository";
    
ads

    const consultations = consultationsRepository.getConsultations().map(v=>
        <ConsultationTerm term={v} key={v.slotId} colClass={"col-md-6 mt-2 col-sm-12"}/>
    );
    
xxx

    <div className="App">
        <Header/>
        <main role="main" className="mt-3">

            <div className="container">
                <div className="row">
                    {consultations}
                </div>
            </div>
        </main>
    </div>
    
dd

    import moment from 'moment';
    import Moment from "react-moment";
    
dd

    constructor(props) {
            super(props);
            let termDate = moment(props.term.date + "T" + props.term.from);
            let now = moment();
            this.state = {
                termDate: termDate,
                timeLeft: termDate - now
            }
        }
    
        componentDidMount() {
            this.timerId = setInterval(this.tick, 1000);
        }
    
        componentWillUnmount() {
            if (this.timerId) {
                clearInterval(this.timerId);
            }
    
        }
    
        tick = () => {
            // this.setState((state, props) => ({
            //     timeLeft: this.state.termDate - new Date()
            // }))
            this.setState((state,props)=> {
                return {
                    timeLeft : this.state.termDate - moment.now()
                }
            })
        }

ddd

    <div className="row">
        <div className="col-md-6 font-weight-bold">Time left:</div>
        <div className="col-md-6"><Moment
            format="DD hh:mm:ss">{this.state.timeLeft}</Moment></div>
    </div>