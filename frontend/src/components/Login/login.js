import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
const AUTH_TOKEN = 'auth_token';


const loginForm = (props) => {

    const [auth, setAuth] = useState({});

    const history = useHistory();

    const login = (e) => {
        e.preventDefault();

        const auth = {
            "username":e.target.username.value,
            "password":e.target.password.value
        }

        props.login(auth , (response)=>{
            debugger;
            localStorage.setItem(AUTH_TOKEN, response.data);
            history.push("/consultations");
        });

    }

    const onChangeHandler = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        setAuth({name:value});
    }

    return (
        <div>
            <form onSubmit={login}>
                <div className="row form-group">
                    <div className="col-md-6 font-weight-bold"> Корисничко име:</div>
                    <div className="col-md-6">
                        <input name={"username"} onChange={onChangeHandler} defaultValue={auth.username} type="text"
                               className="form-control"/>
                    </div>
                </div>
                <div className="row form-group">
                    <div className="col-md-6 font-weight-bold"> Лозинка:</div>
                    <div className="col-md-6">
                        <input name={"password"} onChange={onChangeHandler} defaultValue={auth.password} type="password"
                               className="form-control"
                               title="Лозинка"/>
                    </div>
                </div>
                <div className="col-md-12 text-right">

                    <button type="submit" className="btn btn-primary" title="Зачувај">
                        <i className="fa fa-fw fa-user"></i> Логин
                    </button>
                </div>
            </form>
        </div>
    )
}

export default loginForm;