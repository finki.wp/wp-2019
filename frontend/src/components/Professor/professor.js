import ConsultationTerm from "../Consultations/ConsultationTerm/consultationTerm";
import React from "react";


const professor = (props) => {


    return (
        <div className={props.colClass}>
            <div className="card">
                {cardHeader()}
                <div className="card-body">
                    <div className="card-text">
                        {consultations()}
                    </div>
                </div>
            </div>
        </div>
    );

    function cardHeader() {
        return (<div className="card-header">
            <div className="row">
                <div className="col-md-6">
                    {props.value.professor.title} {props.value.professor.firstName} {props.value.professor.lastName}
                </div>
                <div className="col-md-6 text-right">
                    <a href="#" className="btn btn-light" title="Следи">
                        <i className="fa fa-star"></i>
                    </a>
                </div>
            </div>
        </div>);

    }


    function consultations() {
        return props.value.slots.map(slot =>
            <ConsultationTerm term={slot} key={slot.slotId}/>
        );
    }
};

export default professor;