import React from 'react';
import Professor from '../../Professor/professor'
import * as consultationsRepository from "../../../repository/consultationsRepository";


const consultations = (props) => {
    return (
        <div>
            <div className="row">
                {consultations()}
            </div>
            {pagination()}
        </div>
    );

    function consultations() {
        return consultationsRepository.getConsultations().map(professor =>
            <Professor value={professor} key={professor.id} colClass={"col-md-6 mt-2 col-sm-12"}/>
        );
    }

    function pagination() {

        return (
            <nav aria-label="Page navigation example" className="mt-5">
                <ul className="pagination justify-content-center">
                    <li className="page-item"><a className="page-link" href="#">Previous</a></li>
                    <li className="page-item"><a className="page-link" href="#">1</a></li>
                    <li className="page-item"><a className="page-link" href="#">2</a></li>
                    <li className="page-item"><a className="page-link" href="#">3</a></li>
                    <li className="page-item"><a className="page-link" href="#">Next</a></li>
                </ul>
            </nav>
        );
    }
};

export default consultations;
