import React from 'react'
import {useHistory} from 'react-router-dom';

const consultationAdd = (props) => {

    const history = useHistory();

    const onFormSubmit = (e) => {
        e.preventDefault();
        const newTerm = {
            "professor": {
                "id": "kostadin.mishev",
                "title": "м-р",
                "firstName": "Костадин",
                "lastName": "Мишев",
                "followers": [
                    {
                        "index": "170001",
                        "firstName": "Petko",
                        "lastName": "Petkovski"
                    }
                ]
            },
            "room": {
                "name": e.target.roomName.value
            },
            "dayOfWeek": e.target.dayOfWeek.value,
            "from": e.target.from.value,
            "to": e.target.to.value
        };
        props.onNewTermAdded(newTerm);
        history.push("/consultations");

    }

    return (
        <div>
            <div className="card-body">
                <div className="card-text">
                    <div className="consultations">
                        <form onSubmit={onFormSubmit}>
                            <div className="row form-group">
                                <div className="col-md-6 font-weight-bold"> Ден:</div>
                                <div className="col-md-6">
                                    <select name={"dayOfWeek"} className="form-control">
                                        <option value={"MONDAY"}>Понеделник</option>
                                        <option value={"TUESDAY"}>Вторник</option>
                                        <option value={"WEDNESDAY"}>Среда</option>
                                        <option value={"THURSDAY"}>Четврток</option>
                                        <option value={"FRIDAY"}>Петок</option>
                                        <option value={"SATURDAY"}>Сабота</option>
                                        <option value={"SUNDAY"}>Недела</option>
                                    </select>
                                </div>
                            </div>
                            <div className="row form-group">
                                <div className="col-md-6 font-weight-bold"> Време:</div>
                                <div className="col-md-6">
                                    <div className="row">
                                        <div className="col-md-5">
                                            <input name={"from"} type="text"
                                                   className="form-control"
                                                   pattern="\d\d:?\d\d"
                                                   title="Време од"/>
                                        </div>
                                        <div className="col-md-2 text-center">
                                            -
                                        </div>
                                        <div className="col-md-5 text-right">
                                            <input name={"to"} type="text"
                                                   pattern="\d\d:?\d\d"
                                                   className="form-control"
                                                   title="Време до"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row form-group">
                                <div className="col-md-6 font-weight-bold"> Просторија</div>
                                <div className="col-md-6">
                                    <select name={"roomName"} className="form-control">
                                        <option>123(Ф)</option>
                                        <option>223(М)</option>
                                        <option>B1</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-12 text-right">

                                <button type="submit" className="btn btn-primary" title="Зачувај">
                                    <i className="fa fa-fw fa-save"></i> Зачувај
                                </button>
                            </div>
                        </form>
                        <hr/>
                    </div>
                </div>
            </div>

        </div>
    )
}

export default consultationAdd;