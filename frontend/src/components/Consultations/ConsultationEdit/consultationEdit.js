import React,{useState,useEffect} from 'react'
import {useParams, useHistory} from 'react-router-dom';
import ConsultationsService from "../../../repository/axiosConsultationsRepository";

const consultationEdit = (props) => {

    const [term,setTerm] = useState({});
    const {termId} = useParams();
    const history = useHistory();

    useEffect(() => {
        ConsultationsService.fetchById(termId).then((response)=>{
            const term = response.data;
            const newTerm = {
                ...term,
                "roomName":term.room.name
            }
            setTerm(newTerm);
        })
    },[])

    const onFormSubmit = (e) => {
        e.preventDefault();
        props.onSubmit(
            {
            "slotId":termId,
            "room": {
                "name": e.target.roomName.value
            },
            "dayOfWeek": e.target.dayOfWeek.value,
            "from": e.target.from.value,
            "to": e.target.to.value
        }
        );

        history.push("/consultations");

    }

    const handleTermOnChange  = (e) => {
        const paramName = e.target.name;
        const paramValue = e.target.value;
        setTerm({paramName:paramValue});
    }

    return (
        <div>
            <div className="card-body">
                <div className="card-text">
                    <div className="consultations">
                        <form onSubmit={onFormSubmit}>
                            <div className="row form-group">
                                <div className="col-md-6 font-weight-bold"> Ден:</div>
                                <div className="col-md-6">
                                    <select value={term.dayOfWeek} onChange={handleTermOnChange} name={"dayOfWeek"} className="form-control">
                                        <option key={"MONDAY"} value={"MONDAY"}>Понеделник</option>
                                        <option key={"TUESDAY"} value={"TUESDAY"}>Вторник</option>
                                        <option key={"WEDNESDAY"} value={"WEDNESDAY"}>Среда</option>
                                        <option key={"THURSDAY"} value={"THURSDAY"}>Четврток</option>
                                        <option key={"FRIDAY"} value={"FRIDAY"}>Петок</option>
                                        <option key={"SATURDAY"} value={"SATURDAY"}>Сабота</option>
                                        <option key={"SUNDAY"} value={"SUNDAY"}>Недела</option>
                                    </select>
                                </div>
                            </div>
                            <div className="row form-group">
                                <div className="col-md-6 font-weight-bold"> Време:</div>
                                <div className="col-md-6">
                                    <div className="row">
                                        <div className="col-md-5">
                                            <input onChange={handleTermOnChange} name={"from"} type="text"
                                                   className="form-control"
                                                   pattern="\d\d:?\d\d"
                                                   defaultValue={term.from}
                                                   title="Време од"/>
                                        </div>
                                        <div className="col-md-2 text-center">
                                            -
                                        </div>
                                        <div className="col-md-5 text-right">
                                            <input onChange={handleTermOnChange}  name={"to"} type="text"
                                                   pattern="\d\d:?\d\d"
                                                   defaultValue={term.to}
                                                   className="form-control"
                                                   title="Време до"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row form-group">
                                <div className="col-md-6 font-weight-bold"> Просторија</div>
                                <div className="col-md-6">
                                    <select value={term.roomName} onChange={handleTermOnChange} name={"roomName"} className="form-control">
                                        <option>123(Ф)</option>
                                        <option>223(М)</option>
                                        <option>B1</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-12 text-right">

                                <button type="submit" className="btn btn-primary" title="Зачувај">
                                    <i className="fa fa-fw fa-save"></i> Зачувај
                                </button>
                            </div>
                        </form>
                        <hr/>
                    </div>

                </div>
            </div>

        </div>
    )
}

export default consultationEdit;