import React, {Component} from 'react';
import Moment from "react-moment";
import {Link} from "react-router-dom";


class ConsultationTerm extends Component {

    render() {

        return (
            <div className={this.props.colClass}>
                <div className="card">
                    <div className="consultations">
                        {this.cardHeader()}
                        {this.slotDayOrDate()}
                        {this.slotTime()}
                        {this.slotRoom()}
                        <hr/>
                    </div>
                </div>
            </div>
        );
    }


    cardHeader() {
        return (
            <div className="card-header">
                <div className="row">
                    <div className="col-md-6">
                        {this.props.term.professor.title} {this.props.term.professor.firstName} {this.props.term.professor.lastName}
                    </div>
                    <div className="col-md-6 text-right">
                        <a href="#" className="btn btn-light" title="Следи">
                            <i className="fa fa-star"></i>
                        </a>
                        <Link className="btn btn-default" to={"/consultations/"+this.props.slotId+"/edit"}><i className="fa fa-pencil"></i></Link>
                        <a onClick={()=>this.props.onDelete(this.props.slotId)} className="btn btn-danger" title="Избриши">
                            <i className="fa fa-trash"></i>
                        </a>
                    </div>

                </div>
            </div>);
    }

    slotDayOrDate() {

        if (this.props.term.dayOfWeek) {
            return (
                <div className="row">
                    <div className="col-md-6 font-weight-bold"> Ден:</div>
                    <div className="col-md-6">{this.props.term.dayOfWeek}</div>
                </div>
            );
        } else if (this.props.term.date) {
            return (
                <div>
                    <div className="row">
                        <div className="col-md-6 font-weight-bold">Датум:</div>
                        <div className="col-md-6"><Moment
                            format="DD/MM/YYYY">{this.props.term.date}</Moment></div>
                    </div>

                </div>
            );
        } else {
            return null
        }
    }

    slotTime() {
        return (
            <div className="row">
                <div className="col-md-6 font-weight-bold">Време:</div>
                <div className="col-md-6">
                    <Moment format={"hh:mm"} parse={"hh:mm:ss"}>
                        {this.props.term.from}
                    </Moment>-
                    <Moment parse={"hh:mm:ss"} format={"hh:mm"}>
                        {this.props.term.to}
                    </Moment></div>
            </div>
        );
    }

    slotRoom() {
        return (
            <div className="row">
                <div className="col-md-6 font-weight-bold">Просторија</div>
                <div className="col-md-6">
                    <a href="/Home/Rooms">{this.props.term.room.name}</a>
                </div>
            </div>
        );
    }
}


export default ConsultationTerm;