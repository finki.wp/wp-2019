import React from 'react';
import ConsultationTerm from "../ConsultationTerm/consultationTerm";
import ReactPaginate from 'react-paginate';

const getConsultations = (props) => {
    const consultationTerms = props.terms.map((term, index) => {
        return (
            <ConsultationTerm onDelete={props.onDelete} slotId={term.slotId} term={term} key={index}
                              colClass={"col-md-6 mt-2 col-sm-12"}/>
        );
    });

    const handlePageClick = (e) => {
        props.onPageClick(e.selected)
    }

    const paginate = () => {
        if (props.totalPages !== 0) {
            return (
                <ReactPaginate previousLabel={"previous"}
                               nextLabel={"next"}
                               breakLabel={<span className="gap">...</span>}
                               breakClassName={"break-me"}
                               pageCount={props.totalPages}
                               marginPagesDisplayed={2}
                               pageRangeDisplayed={5}
                               pageClassName={"page-item"}
                               pageLinkClassName={"page-link"}
                               previousClassName={"page-item"}
                               nextClassName={"page-item"}
                               previousLinkClassName={"page-link"}
                               nextLinkClassName={"page-link"}
                               forcePage={props.page}
                               onPageChange={handlePageClick}
                               containerClassName={"pagination justify-content-center"}
                               activeClassName={"active"}/>
            )
        }
    }
    return (
        <div>
            <div className={"row"}>
                {consultationTerms}
            </div>
            {paginate()}
        </div>
    )
}

export default getConsultations;