import React, {Component} from 'react';
import './App.css';
import Header from "../Header/header";
import Consultations from "../Consultations/ConsultationList/consultations"
import ConsultationAdd from "../Consultations/ConsultationAdd/consultationAdd"
import ConsultationEdit from "../Consultations/ConsultationEdit/consultationEdit"
import {BrowserRouter as Router, Redirect, Route} from 'react-router-dom'
import ConsultationService from '../../repository/axiosConsultationsRepository';
import LoginForm from "../Login/login";




class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            terms: [],
            pageSize:2,
            page:0,
            totalPages:0
        }
    }

    componentDidMount() {
        this.loadConsultations();
    }

    loadConsultations = (page=0) => {
        ConsultationService.fetchConsultationTermsPaged(page,this.state.pageSize).then((data) => {
            this.setState({
                terms: data.data.content,
                page:data.data.page,
                pageSize:data.data.pageSize,
                totalPages:data.data.totalPages
            })
        })
    }

    createConsultation = (newTerm) => {
        ConsultationService.addConsultationTerm(newTerm).then((response)=>{
            const newTerm = response.data;
            this.setState((prevState) => {
                const newTermsRef = [...prevState.terms, newTerm];
                //or
                //const terms = prevState.terms.concat(newTerm);
                return {
                    "terms": newTermsRef
                }
            });
        });
    }

    updateConsultation = ((editedTerm) => {
        ConsultationService.updateConsultationTerm(editedTerm).then((response)=>{
            const newTerm = response.data;
            this.setState((prevState) => {
                const newTermsRef = prevState.terms.map((item)=>{
                    if (item.slotId===newTerm.slotId) {
                        return newTerm;
                    }
                    return item;
                })
                return {
                    "terms": newTermsRef
                }
            });
        });
    });

    deleteConsultation = (slotId) => {
        ConsultationService.deleteConsultationTerm(slotId).then((response)=>{
            this.setState((state) => {
                const terms = state.terms.filter((t) => {
                    return t.slotId !== slotId;
                });
                return {terms}
            })
        })
    }

    searchData = (searchTerm) => {
        ConsultationService.searchConsultationTerm(searchTerm).then((response)=>{
            this.setState({
                terms: response.data,
                page:0,
                pageSize:0,
                totalPages:0
            })
        })
    }

    login = (authInfo, callback) => {
        ConsultationService.login(authInfo).then((response) => {
            callback(response);
        })
    }

    render() {

        const routing = (
            <Router>
                <Header onSearch={this.searchData}/>

                <main role="main" className="mt-3">

                    <div className="container">
                        <Route path={"/consultations"} exact render={()=>
                            <Consultations onPageClick={this.loadConsultations} terms={this.state.terms} onDelete={this.deleteConsultation} totalPages={this.state.totalPages}/>}>

                        </Route>
                        <Route path={"/consultations/add"} render={()=><ConsultationAdd onNewTermAdded={this.createConsultation}/>}>
                        </Route>
                        <Route path="/consultations/:termId/edit" render={()=>
                            <ConsultationEdit onSubmit={this.updateConsultation}/>}>
                        </Route>
                        <Route path={"/login"} exact
                               render={() => <LoginForm login={this.login}/>}>
                        </Route>
                        <Redirect to={"/consultations"}/>
                    </div>
                </main>
            </Router>
        )

        return (

            <div className="App">
                {routing}
            </div>
        );
    }
}

export default App;
