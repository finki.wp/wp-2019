import axios from '../custom-axios/axios'
import qs from 'qs'

const ConsultationsService = {
    fetchConsultationTerms: ()=> {
        return axios.get("/api/consultations");
    },
    fetchConsultationTermsPaged:(page,pageSize)=>{
        return axios.get("/api/consultations",{
            headers: {
                'page':page,'page-size':pageSize
            }
        })
    },
    addConsultationTerm: (term) => {
        const data = {
            ...term,
            roomName:term.room.name
        }
        const formParams = qs.stringify(data);
        return axios.post("/api/consultations",formParams, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'professorId':'kostadin.mishev'
            }
        });
    },
    updateConsultationTerm : (term) => {
        const data = {
            ...term,
            roomName:term.room.name
        }
        const slotId= term.slotId;
        const formParams = qs.stringify(data);
        return axios.patch(`/api/consultations/${slotId}`,formParams, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'professorId':'kostadin.mishev'
            }
        });
    },
    fetchById : (slotId) => {
        return axios.get(`/api/consultations/${slotId}`);
    },
    deleteConsultationTerm: (slotId) => {
        return axios.delete(`/api/consultations/${slotId}`);
    },
    searchConsultationTerm: (searchTerm) => {
        return axios.get(`/api/consultations?term=${searchTerm}`);
    },
    login:(auth) => {
        return axios.post("/login",auth);
    }
}

export default ConsultationsService;